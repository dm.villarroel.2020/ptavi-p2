#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys

import computecount

compute = computecount.Compute()
ops = {
    'power': compute.power,
    'log': compute.log,
}

def process_op(line):
    contents = line.rstrip().split(',')
    if (len(contents) < 2) or (len(contents) > 3):
        raise ValueError
    else:
        operand = contents.pop(0)
        num = float(contents.pop(0))
        if len(contents) == 0:
            num2 = None
        else:
            num2 = float(contents.pop(0))
        result = ops[operand](num, num2)
        print(result)


def process_csv(filename):
    with open(filename, 'r') as file:
        first_line = True
        for line in file:
            if first_line:
                try:
                    compute.set_def(float(line))
                except:
                    compute.set_def(2)
                print(f"Default: {compute.get_def()}")
                first_line = False
            else:
                try:
                    process_op(line)
                except (ValueError, KeyError):
                    print("Bad format")
    print(f"Operations: {compute.count}")


if __name__ == "__main__":
    try:
        filename = sys.argv[1]
    except IndexError:
        print("Error: Execute with the name of file to process as argument")
        sys.exit(-1)
    try:
        process_csv(filename)
    except FileNotFoundError:
        print(f"Error: File {filename} does not exist")
        sys.exit(-1)