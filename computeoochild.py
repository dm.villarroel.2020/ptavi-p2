#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys

import computeoo

class ComputeChild(computeoo.Compute):

    def sef_def(self, default): #potencia o log no especifíca base o exp, se usa valor default
        if default <= 0:
            raise ValueError
        self.default = default


    def get_def(self):
        return self.default

if __name__ == "__main__":
    if len(sys.argv) < 3:
        sys.exit("Sorry, at leats two numbers are needed")

    try:
        num = float(sys.argv[2])
    except ValueError:
        sys.exit("Error: second argument should be a number")

    if len(sys.argv) == 3:
        num2 = None
    else:
        try:
            num2 = float(sys.argv[3])
        except ValueError:
            sys.exit("Error: trid argument should be a number")

    comp = ComputeChild()
    comp.set_def(num2)
    if sys.argv[1] == 'power':
        result = comp.power(num)
        print(result)
    elif sys.argv[1] == 'log':
        result = comp.log(num)
        print(result)
    else:
        sys.exit('Operand should be power or log')

