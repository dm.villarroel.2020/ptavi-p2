#!/usr/bin/python3
# -*- coding: utf-8 -*-

import math
import sys

class Compute:
    pass
    def __init__(self):
        self.default = 2

    def power(self, num, exp=None):
        """ Function to compute number to the exp power"""
        if exp is None:
            exp = self.default
        return num ** exp

    def log(self, num, base=None):
        """ Function to compute log base of number"""
        if base is None:
            base = self.default
        return math.log(num, base)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        sys.exit("Error: at least two arguments are needed")

    try:
        num = float(sys.argv[2])
    except ValueError:
        sys.exit("Error: second argument should be a number")

    try:
        num = float(sys.argv[2])
    except ValueError:
        sys.exit('Error: second argument should be a number')

    if len(sys.argv) == 3:
        num2 = None
    else:
        try:
            num2 = float(sys.argv[3])
        except ValueError:
            sys.exit("Error: third argument should be a number")

comp = Compute()
if sys.argv[1] == "power":
    result = comp.power(num, num2)
elif sys.argv[1] == "log":
    result = comp.log(num, num2)
else:
    sys.exit('Operand should be power or log')

print(result)